class Post < ActiveRecord::Base
  has_many :comments, dependent: :destroy  #When a post is deleted, all of its comments are deleted too
  validates_presence_of :title
  validates_presence_of :body
end
